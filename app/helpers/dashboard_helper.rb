module DashboardHelper
  def numbers_array(start_point, end_point, step)
    i = start_point
    num = end_point
    arr = []
    begin
      i = i.round(4)
      arr << i
      i += step
    end while i <= num
    arr
  end

  def fx_1(x)
    (4 / x).round(4)
  end

  def fx_2(x)
    ((5 + 6 * x - Math.sqrt(x**3)) / 12).round(4)
  end

  def f_12(x1, x2)
    (x1**3 - x2**3).round(4)
  end

  def f_21(x1, x2)
    (x1 + x2).round(4)
  end

  def generate_hash(params)
    arr = []
    x1_arr = numbers_array(params[:start_point], params[:end_point], params[:h1])
    x2_arr = numbers_array(params[:start_point], params[:end_point], params[:h2])
    fx = params[:fx]
    x1_arr.each do |x1|
      h = Hash.new
      x2_arr.each do |x_current|
        h.merge!({f_12(x1, x_current) => [x1, x_current]}) if fx == 12
        h.merge!({f_21(x1, x_current) => [x1, x_current]}) if fx == 21
      end
      min = h.keys.min
      min_x = h.select{|k, v| k == min}
      arr.push({x1: x1, x2: x2_arr, fx: h.keys, min_fx: min, min_fx_x1_x2: min_x })
    end
    arr
  end
end
