window.onload = function() {
    fxx(12);
    fxx(21);

    function fxx(vers){
        if(vers == 12) {
            ox = $('.fx-12').data('ox');
            fx = $('.fx-12').data('fx');
            x1 = $('.fx-12').data('x1');
            container_name = 'container'
        }
        if(vers == 21) {
            ox = $('.fx-21').data('ox');
            fx = $('.fx-21').data('fx');
            x1 = $('.fx-21').data('x1');
            container_name = 'container21'
        }

        var arr = []
        fx.forEach(function(item, i){
            arr.push({
                name: 'X1 = ' + x1[i],
                data: item
            })
        });
        console.log(arr)
        Highcharts.chart(container_name, {

            title: {
                text: 'Solar Employment Growth by Sector, 2010-2016'
            },

            yAxis: {
                title: {
                    text: 'Number of Employees'
                }
            },
            xAxis: {
                allowDecimals: true
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            xAxis: {
                categories: ox
            },

            series: arr,

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    }

}


