Rails.application.routes.draw do
  root to: 'dashboard#index'
  get '/second_task', to: 'dashboard#second_task', via: :get
end
